package com.wlw.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Properties;

import com.wlw.bean.Paijianruku;

public class PaijianrukuDao {

	public ArrayList<Paijianruku> query() {
		ArrayList<Paijianruku> paijianrukuArr = new ArrayList<Paijianruku>();
		Connection conn = null;
		Properties properties = new Properties();
		properties.setProperty("user", "root");
		properties.setProperty("password", "123456");
		properties.setProperty("useSSL", "false");
		properties.setProperty("autoReconnect", "true");
		try {
		
			Class.forName("com.mysql.jdbc.Driver");

			 conn = DriverManager.getConnection("jdbc:mysql://192.168.0.100:3306/bigdata_wlw?useUnicode=true&characterEncoding=utf-8",
						properties);
			PreparedStatement stmt = conn.prepareStatement("SELECT * FROM t_paijianruku");
			ResultSet rs = stmt.executeQuery();
		
			while(rs.next()) {
				Paijianruku paijianruku = new Paijianruku();
				paijianruku.setName(rs.getString("name"));
				paijianruku.setValue(rs.getInt("value"));
				paijianruku.setPercent(rs.getString("percent"));
				paijianrukuArr.add(paijianruku);
			}
		
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return paijianrukuArr;
	}

}
