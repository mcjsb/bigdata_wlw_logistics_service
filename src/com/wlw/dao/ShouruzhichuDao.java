package com.wlw.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.wlw.bean.Shouruzhichu;
import com.wlw.db_util.ConnectHelper;

public class ShouruzhichuDao {
	public ArrayList<Shouruzhichu>  query() {
		ArrayList<Shouruzhichu> shouruzhichuArr = new ArrayList<Shouruzhichu>();
		ConnectHelper ch = new ConnectHelper();
		Connection conn = ch.openConnection();
		try {
			PreparedStatement ps = conn.prepareStatement("select * from t_shouruzhichu");
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				Shouruzhichu shouruzhichu = new Shouruzhichu();
				shouruzhichu.setShouru(rs.getString("shouru"));
				shouruzhichu.setZhichu(rs.getString("zhichu"));
				shouruzhichuArr.add(shouruzhichu);
				}
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} 
		
		return shouruzhichuArr;
	}
	

}
