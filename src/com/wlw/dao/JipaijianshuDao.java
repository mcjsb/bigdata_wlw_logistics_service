package com.wlw.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

public class JipaijianshuDao {
	public JSONArray  query() {
		JSONArray array = new JSONArray();
		Connection conn = null;
		Properties properties = new Properties();
		properties.setProperty("user", "root");
		properties.setProperty("password", "123456");
		properties.setProperty("useSSL", "false");
		properties.setProperty("autoReconnect", "true");

		try {
			Class.forName("com.mysql.jdbc.Driver");

		 conn = DriverManager.getConnection("jdbc:mysql://192.168.0.100:3306/bigdata_wlw?useUnicode=true&characterEncoding=utf-8",
					properties);
			PreparedStatement stmt = conn.prepareStatement("SELECT * FROM t_jipaijianshu");
			ResultSet rs = stmt.executeQuery();
			
			while (rs.next()) {
				JSONObject jsonObject = new JSONObject();
				String name = rs.getString("name");
				int value = rs.getInt("value");
				jsonObject.put("name", name);
				jsonObject.put("value", value);
				array.add(jsonObject);
			}
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		/*map.put("name", paimingArr1);
		map.put("num", paimingArr2);*/
		return array;
	}

}
