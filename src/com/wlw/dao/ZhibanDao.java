package com.wlw.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.wlw.bean.Zhiban;
import com.wlw.db_util.ConnectHelper;

public class ZhibanDao {
	
	public ArrayList<Zhiban>  query() {
		ArrayList<Zhiban> zhibanArr = new ArrayList<Zhiban>();
		ConnectHelper ch = new ConnectHelper();
		Connection conn = ch.openConnection();
		try {
			PreparedStatement ps = conn.prepareStatement("select * from t_zhiban");
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				Zhiban zhiban= new Zhiban();
				zhiban.setJinrizhiban(rs.getString("jinrizhiban"));
				zhiban.setFuzeren(rs.getString("fuzeren"));
			    zhibanArr.add(zhiban);
				}
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} 
		
		return zhibanArr;
	}

}
