package com.wlw.dao;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import com.wlw.bean.Daojianliang;
import com.wlw.db_util.ConnectHelper;

public class DaojianliangDao { 
	public ArrayList<Daojianliang>  query() {
		ArrayList<Daojianliang> daojianliangArr = new ArrayList<Daojianliang>();
		ConnectHelper ch = new ConnectHelper();
		Connection conn = ch.openConnection();
		try {
			PreparedStatement ps = conn.prepareStatement("select * from t_daojianliang");
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				Daojianliang daojianliang = new Daojianliang();
				daojianliang.setDaojianliang(rs.getInt("daojianliang"));
				daojianliangArr.add(daojianliang);
				
			}
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} 
		
		return daojianliangArr;
	}
	


}
