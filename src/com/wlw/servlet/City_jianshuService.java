package com.wlw.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.wlw.dao.City_jianshuDao;

import net.sf.json.JSONArray;

public class City_jianshuService extends HttpServlet{

	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		//创建DAO
		City_jianshuDao city_jianshuDao= new City_jianshuDao();
		//从数据库里取数据
		JSONArray city_jianshuArr = city_jianshuDao.query();
		//设置服务器响应时向JSP表示层传输数据的编码格式
		resp.setContentType("text/html; charset=utf-8");
		//ArrayList对象转化为JSON对象
		JSONArray json = JSONArray.fromObject(city_jianshuArr);
		//控制台显示JSON
		System.out.println(json.toString());
		//返回到JSP
		PrintWriter writer = resp.getWriter();
		writer.println(json);
		writer.flush();
		//关闭输出流
		writer.close();
	}

}
