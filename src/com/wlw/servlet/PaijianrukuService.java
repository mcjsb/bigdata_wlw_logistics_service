package com.wlw.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.wlw.bean.Paijianruku;
import com.wlw.dao.PaijianrukuDao;

import net.sf.json.JSONArray;

public class PaijianrukuService extends HttpServlet{

	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		//创建DAO
		PaijianrukuDao paijianrukuDao = new PaijianrukuDao();
		
		//从数据库里取数据
		ArrayList<Paijianruku> paijianrukuArr = paijianrukuDao.query();
		//设置服务器响应时向JSP表示层传输数据的编码格式
		resp.setContentType("text/html; charset=utf-8");
		//ArrayList对象转化为JSON对象
		JSONArray json = JSONArray.fromObject(paijianrukuArr);
		//控制台显示JSON
		System.out.println(json.toString());
		//返回到JSP
		PrintWriter writer = resp.getWriter();
		writer.println(json);
		writer.flush();
		//关闭输出流
		writer.close();
	}
}
