package com.wlw.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.omg.CORBA.PUBLIC_MEMBER;

import com.wlw.dao.JipaijianshujuDao;


import net.sf.json.JSONArray;

public class JipaijianshujuService extends HttpServlet{
	public static String type;
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		 type=req.getParameter("type");
		System.out.println(type);
		JipaijianshujuDao jipaijianshujudao = new JipaijianshujuDao();
		JSONArray jipaijianshujuArr= jipaijianshujudao.query(type);
		resp.setContentType("text/html; charset=utf-8");
		//ArrayList对象转化为JSON对象
		JSONArray json = JSONArray.fromObject(jipaijianshujuArr);
		//控制台显示JSON
		System.out.println(json.toString());
		//返回到JSP
		PrintWriter writer = resp.getWriter();
		writer.println(json);
		writer.flush();
		//关闭输出流
		writer.close();
	}
	
}
