package com.wlw.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.wlw.bean.Zhiban;
import com.wlw.dao.ZhibanDao;

import net.sf.json.JSONArray;

@WebServlet("/ZhibanServlet")
public class ZhibanServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
  
  
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ZhibanDao zhibanDao = new ZhibanDao();
   		ArrayList<Zhiban> zhibanArr = zhibanDao.query();
   		response.setContentType("text/html;charset=utf-8");
   		JSONArray json = JSONArray.fromObject(zhibanArr);
   		PrintWriter writer = response.getWriter();
   		writer.println(json);
   		writer.flush();
   		writer.close();
	}

}
