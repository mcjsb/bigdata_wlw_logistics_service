package com.wlw.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.wlw.bean.Daojianliang;
import com.wlw.dao.DaojianliangDao;

import net.sf.json.JSONArray;

@WebServlet("/DaojianliangServlet")
public class DaojianliangServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		DaojianliangDao daojianliangDao = new DaojianliangDao();
		//从数据库里取数据
		ArrayList<Daojianliang> daojianliangArr = daojianliangDao.query();
		//设置服务器响应时向JSP表示层传输数据的编码格式
		response.setContentType("text/html; charset=utf-8");
		//ArrayList对象转化为JSON对象
		JSONArray json = JSONArray.fromObject(daojianliangArr);
		//控制台显示JSON
//		System.out.println(json.toString());
		//返回到JSP
		PrintWriter writer = response.getWriter();
		writer.println(json);
		writer.flush();
		//关闭输出流
		writer.close();
	}

}
