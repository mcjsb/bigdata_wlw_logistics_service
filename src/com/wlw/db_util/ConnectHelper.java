package com.wlw.db_util;

import java.sql.Connection;
import java.sql.DriverManager;

public class ConnectHelper {
	public Connection openConnection() {
		Connection conn = null;
		try {
			Class.forName("com.mysql.jdbc.Driver");
			conn = DriverManager.getConnection(
//					"jdbc:mysql://192.168.0.100:3306/db_1?useUnicode=true&characterEncoding=utf-8", "root", "123456");
					"jdbc:mysql://192.168.0.100:3306/bigdata_wlw?useUnicode=true&characterEncoding=utf-8","root","123456");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return conn;
	}
}
